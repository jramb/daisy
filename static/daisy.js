//2016-2017 by J Ramb

Date.prototype.toJSONLocal = Date.prototype.toJSONLocal ||
  function () {
    var local = this;//new Date(date);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0, 19).replace("T"," ");
  }



// main function, run when the document is ready
$(function() {
  //alert("ready!");

  var doVisualUpdates = true;
  document.addEventListener('visibilitychange', function(){
    doVisualUpdates = !document.hidden;
  });

  var ds = new Vue({
    el: '#daisyStats',
    data:
    {
      initialized: false,
      alive: false,
      frequency: 5000,
      stats: {
        StartupTime: "2000-01-01",
        refreshDate: new Date(),
        CurrentCounters: {}
      }
    }
  })

  var timeoutId;

  function loadStats() {
    ds.alive = false;
    if (doVisualUpdates) {
      $.ajax("/wui/stats")
        .done(function( data ) {
          ds.stats = data;
          ds.stats.refreshDate = new Date();
          ds.initialized = true;
          ds.alive = true;
          if (ds.frequency>0) {
            //        timeoutId=setTimeout(loadStats, ds.frequency);
          }
        })
      .fail(function() { // does not catch CONNECTION_REFUSED??
        //ds.initialized = false;
      });
    }
  }

  loadStats();
  setInterval(loadStats, 5000);

});

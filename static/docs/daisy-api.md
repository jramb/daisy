% Daisy REST API Description
% Jörg Ramb
% 2016-12-06

This is a description of the way the Daisy REST interface works.

## Internal/Admin interface
This interface is used internally to create and maintain **Sources**.  A
*Source* is a single file or URL whose contents can be accessed from the
outside.

The *Source-ID* is to be kept secret, because having access to it allows the
modification of the sources meta-data, such as number of handles, date of
validity and of course access to the contents itself.

A Source can have many **Access handlers**.  Each *access handler* is a unique
way to access the *source*, access is logged per single *access handler*.
Access handlers are not secret per-se, as you cannot do anything with these
handlers apart from fetching the contents (logged).

By sharing the access handlers (URLs) individually you also have individual
control over the contents accesses.  You can end-date individual accesses and
see which accesses have been accessed -- and how often.

End-dating the source itself will prevent access to all individual access
handlers.


### Creating a new source (binary)

First you have to get the SHA256 checksum of your binary. Your sending
application also needs to know the **api-key**, which is a secret passwort set
in *Daisys* `daisy.toml` setup file.

Sources can specify start and end date of the source itself during creation.
All dates are date and time.
Note that start date defaults to the current date and time. The end date defaults
to what is defined in Daisy's setup, default is 3 months. It is not possible to create
sources with missing start or end date, however the dates specified during creation
are not limited.


The source is created by performing a **POST** to `/source/`:

```bash
curl http://localhost:8090/source/ \
  -H "Content-Type: application/json" -d '
   {"sha256":  "bb0dd477883bb5b0ce30fd7f1d1198141d881ee26d201581ab3dd672edad374f",
    "mime-type": "application/pdf",
    "file-name": "testfile.pdf",
    "apikey": "secretapikey",
    "num-handlers": 2}'
```

This results in something like this:

```json
{
  "source": "IYWN1Mr1QuSYy7ZPg4v6Rw",
  "binary": false,
  "mime-type": "application/octet-stream",
  "file-name": "test.rnd",
  "start-date": "2016-10-21 13:24:53",
  "end-date": "2016-11-10 00:00:00",
  "sha256": "e0dde750c86e65d8ce53692e2ec2d818b5674f2f5768a480a59e4dd8d598136b",
  "num-handlers": 2,
  "access-handlers": [
    {
      "access": "JDB8G11CTuCvb2KHEBTJvQ",
      "url-path": "/g/JDB8G11CTuCvb2KHEBTJvQ",
      "fetch-count": 0
    },
    {
      "access": "Wzw9Ap8wTe6l1S_3bqv9MA",
      "url-path": "/g/Wzw9Ap8wTe6l1S_3bqv9MA",
      "fetch-count": 0
    }
  ]
}
```

**Important**: you must make a note of the `source` (=*source ID*)!
There is no way using the REST to search for forgotten source IDs.


#### Upload the binary

In the case of binaries you now need to upload a file with that specified *sha256*.
Sending a file with a missmatching sha256 will be rejected.

This is done using a **PUT** on the `source/<sourceid>/binary` path:

```bash
curl -T testfile.pdf http://localhost:8090/source/IYWN1Mr1QuSYy7ZPg4v6Rw/binary
```

#### Download the binary

If enabled, the binary can be downloaded by a **GET** 

```bash
curl http://localhost:8090/source/IYWN1Mr1QuSYy7ZPg4v6Rw/binary
```

### Create a new URL type sources
URL type sources are similar to binary sources, except that you don't need to
specify `sha256`, `file-name` or `mime-type`, but instead `url`:

```bash
curl http://localhost:8090/source/ \
  -H "Content-Type: application/json" -d '
   { "apikey": "secretapikey",
     "url": "http://www.google.se",
     "num-handlers": 2}'
```

No binary is uploaded afterwards.


### Update of sources

You can perform these operations on sources:

1. update the start and end dates of the source itself
2. update the start and end dates of individual access IDs
3. Request additional access handlers (by sending a higher `num-handlers` number)

You can't:

1. Not: remove the start or end dates (but you can set an end date long in the future)
2. Not: remove individual access handlers (but you can end-date them!)
3. Not: update the binary
4. Not: change file-name, url or mime-type


Update the source by performing a **POST** on `/source/<source ID>`:

For example to increase the number of access handlers and adjust the dates
at the same time:

```bash
curl http://localhost:8090/source/IYWN1Mr1QuSYy7ZPg4v6Rw \
   -H "Content-Type: application/json" -d '
   {
     "start-date":  "2016-01-13 00:00:00",
     "end-date": "2016-12-25 15:04:05",
     "apikey": "secretapikey",
     "num-handlers": 3}'
```

or to change the end date of an access handler (you can update several
at the same time):

```bash
curl http://localhost:8090/source/IYWN1Mr1QuSYy7ZPg4v6Rw \
  -H "Content-Type: application/json" -d '
  {"apikey": "secretapikey",
   "access-handlers": [{"access": "JDB8G11CTuCvb2KHEBTJvQ",
                        "end-date": "2016-08-27 12:34:56"}]}'
```



### Checking status of a source
By performing a simple **GET** on the source URL you will get the current status of the source and all its handlers.

```bash
curl http://localhost:8090/source/IYWN1Mr1QuSYy7ZPg4v6Rw
```

The result again looks like this:

```json
{
  "source": "IYWN1Mr1QuSYy7ZPg4v6Rw",
  "binary": true,
  "mime-type": "application/octet-stream",
  "file-name": "test.rnd",
  "start-date": "2016-10-21 13:24:53",
  "end-date": "2016-11-10 00:00:00",
  "sha256": "e0dde750c86e65d8ce53692e2ec2d818b5674f2f5768a480a59e4dd8d598136b",
  "num-handlers": 2,
  "access-handlers": [
    {
      "access": "JDB8G11CTuCvb2KHEBTJvQ",
      "url-path": "/g/JDB8G11CTuCvb2KHEBTJvQ",
      "fetch-count": 0
    },
    {
      "access": "Wzw9Ap8wTe6l1S_3bqv9MA",
      "url-path": "/g/Wzw9Ap8wTe6l1S_3bqv9MA",
      "fetch-count": 0
    }
  ]
}
```

### Fetching log of a source
Again using a simple **GET** on the source URL with the sub target `/log` returns a text representation of the source IDs log,
a list of relevant events (creation, fetched,..):

```bash
curl http://localhost:8090/source/IYWN1Mr1QuSYy7ZPg4v6Rw/log
```

The result might be:

```
2016-12-06 14:39:53 | 127.0.0.1:62737 | POST | /source/ | created
2016-12-06 14:39:58 | 127.0.0.1:62761 | GET | /g/JDB8G11CTuCvb2KHEBTJvQ | fetched
2016-12-06 14:41:18 | 127.0.0.1:65000 | GET | /g/Wzw9Ap8wTe6l1S_3bqv9MA | fetched
2016-12-06 14:42:27 | 127.0.0.1:65004 | GET | /g/Wzw9Ap8wTe6l1S_3bqv9MA | fetched
```


## External interface
External users will only receive the access handler, in the above example maybe:

`http://daisy.example.com/g/JDB8G11CTuCvb2KHEBTJvQ`

The access handler (the "secret" in above URL) gives the owner no special
privilege, apart from being able to fetch the contents. You cannot do anything
other with the access handler alone.

Note: For additional security you should restrict external access to only the `/g/` prefix
by firewall/proxy configuration (not part of Daisy).
Only trusted hosts should be allowed to access the other, internal prefixes.

### Get file

When the external part clicks on the access link, Daisy will do several things:

1. Checks that the access ID exists and has not been end-dated.
2. Checks that the corresponding source is not end-dated.
3. Increases the *fetch-count* for this *access ID*
4. Returns the source
    * URL type sources will simply send a redirect
    * Binary type sources will send the binary contents, together with the *mime-type* and *file-name*.
      The user will be prompted to save the file.



Daisy
=====

Welcome to Daisy, a smart and effective proxy file server.

Daisy was built with a specific need in mind (duh!), which might or might not cover exactly
your own needs.


# Installation

    go get gitlab.com/jramb/daisy

# Running

  1) Create `daisy.toml` in the current directory. Refer to the example if necessary.

  2) Run Daisy in your working directory:
  
    daisy

Daisy will listen/serve at the given address (defined in the toml file and printed during startup).
Refer to the API documentation.

You can open http://localhost:8090 for a simple status page with links to the documentation.

Here is a local link to the [documentation](../static/docs/daisy-api.md)

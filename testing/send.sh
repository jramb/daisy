#!/bin/bash

FNAME="$1"
[[ -f $1 ]] || exit
FNAMEFILE=$(basename "${FNAME}")

APIKEY="hejhejleverpastej"
echo "SHAing"
SHA256=$(sha256sum "${FNAME}" | sed 's/\s.*//')
MIMETYPE=$(file --mime-type "${FNAME}" | sed 's/.*\s//')

echo "CURLing ${SHA256}"
result1=$(curl -s \
  http://localhost:8090/source/ \
  -H "Content-Type: application/json" \
  -d @<(cat <<EOF
{"apikey": "${APIKEY}", "sha256": "${SHA256}", "mime-type": "${MIMETYPE}", "file-name": "${FNAMEFILE}", "num-handlers": 2 }
EOF
))
#{"apikey": "${APIKEY}", "sha256": "${SHA256}", "mime-type": "${MIMETYPE}", "file-name": "${FNAMEFILE}", "start-date": "2016-10-31", "num-handlers": 2 }

S=$(echo $result1 | jq -r '.source')
A=$(echo $result1 | jq -r '."access-handlers"[1].access')

#echo $result1
echo S=$S
echo A=$A
echo export S A

echo "PUTing bin"
curl -sT "${FNAME}" http://localhost:8090/source/${S}/binary >/dev/null

echo FETCHing http://localhost:8090/g/${A}

curl  -H 'X-Forwarded-For: 1.2.3.4' -H 'X-Forwarded-Protocol: https' http://localhost:8090/g/${A} >"${FNAME}.check"
echo $SHA256
sha256sum "${FNAME}" "${FNAME}.check"


result2=$(
	curl \
		http://localhost:8090/source/${S} \
		-H "Content-Type: application/json" \
   -d @<(cat <<EOF
		{"apikey": "${APIKEY}", "end-date": "2018-08-01T14:18:57.78807+02:00"}
EOF
))
echo $result2

echo Fetching log: http://localhost:8090/source/${S}/log
curl http://localhost:8090/source/${S}/log

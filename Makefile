FNAME=test-ÄÖÚäöú a.rnd
#FNAME = testfile.pdf
#SHA256=$(sha256sum "$(FNAME)")
SHA256 := $(shell sha256sum "${FNAME}" | sed 's/\s.*//')
MIMETYPE := $(shell file --mime-type ${FNAME} | sed 's/.*\s//')
APIKEY=hejhejleverpastej
NOWDATE=$(shell date +%Y%m%d)

run:
	go run daisy.go

doc:
	pandoc -s static/docs/daisy-api.md -s -o static/docs/daisy-api.html -t html --toc
	pandoc -s static/docs/daisy-api.md    -o static/docs/daisy-api.docx -t docx --toc

win-from-linux:
	env CGO_ENABLED=1 GOOS=windows GOARCH=386 CC="i686-w64-mingw32-gcc -fno-stack-protector -D_FORTIFY_SOURCE=0 -lssp" go build -o daisy.exe

daisy.exe: daisy.go
	go build # daisy.go

win: daisy.exe

package: daisy.exe
	(git status ; git log -1 ) | tee static/docs/daisy.version
	zip -r9 daisy-${NOWDATE}.zip daisy.exe daisy.go nssm.exe static daisy.toml-EXAMPLE

linux:
	go build # daisy.go

post-missing-info:
	@echo "POST"
	curl \
		http://localhost:8090/source/ \
		-H "Content-Type: application/json" \
		-d '{"apikey": "${APIKEY}", "end-date": "2016-11-10 00:00:00", "num-handlers": 2 }'
	@echo "This should fail:" $?

post-new:
	@echo "POST"
	curl \
		http://localhost:8090/source/ \
		-H "Content-Type: application/json" \
		-d '{"apikey": "${APIKEY}", "sha256": "${SHA256}", "mime-type": "${MIMETYPE}", "file-name": "${FNAME}", "end-date": "2016-11-10 00:00:00", "num-handlers": 2 }'
	@echo 
	@echo "Remember to set the environment variable 'S' to the new source"
	@echo "        and set the environment variable 'A' to one of the access codes."

post-url:
	@echo "POST"
	curl \
		http://localhost:8090/source/ \
		-H "Content-Type: application/json" \
		-d '{"apikey": "${APIKEY}", "url": "http://www.exampel.com/document-to-share.pdf", "num-handlers": 2 }'
	@echo "Remember to set the environment variable 'S' to the new source"
	@echo "        and set the environment variable 'A' to one of the access codes."

post-bad-key:
	echo "POST"
	curl \
		http://localhost:8090/source/ \
		-H "Content-Type: application/json" \
		-d '{"apikey": "pleaseletmein", "sha256": "${SHA256}", "mime-type": "${MIMETYPE}", "file-name": "${FNAME}", "num-handlers": 2 }'
	@echo "This should fail:" $?

get:
	echo "GET"
	curl http://localhost:8090/source/${S}


put-bin:
	echo "PUT"
	curl -T ${FNAME} \
		http://localhost:8090/source/${S}/binary

update-source1:
	echo "POST"
	curl \
		http://localhost:8090/source/${S} \
		-H "Content-Type: application/json" \
		-d '{"apikey": "${APIKEY}", "end-date": "2020-10-21T14:18:57.78807+02:00", "num-handlers": 3}'

update-source2:
	echo "POST"
	curl \
		http://localhost:8090/source/${S} \
		-H "Content-Type: application/json" \
		-d '{"apikey": "${APIKEY}", "start-date": "2015-03-25 00:00:00", "end-date": "2020-12-25 12:34:56", "num-handlers": 5}'

update-end:
	echo "POST"
	curl \
		http://localhost:8090/source/${S} \
		-H "Content-Type: application/json" \
		-d '{"apikey": "${APIKEY}", "start-date": "2015-03-25 00:00:00", "end-date": "2016-01-01", "num-handlers": 5}'

update-access1:
	echo "POST"
	curl \
		http://localhost:8090/source/${S} \
		-H "Content-Type: application/json" \
		-d '{"apikey": "${APIKEY}", "access-handlers": [{"access": "${A}", "end-date": "2016-08-27 12:34:56"}]}'


fetch:
	echo "GET"
	touch ${FNAME}.check
	rm ${FNAME}.check
	curl \
		http://localhost:8090/g/${A} -o ${FNAME}.check
	sha256sum ${FNAME} ${FNAME}.check

fetch-log:
	curl \
		http://localhost:8090/source/${S}/log

raw:
	echo Requesting $(SHA256)
	curl -s http://localhost:8090/raw/$(SHA256) | sha256sum

test.rnd:
	dd if=/dev/urandom of="$(FNAME)" bs=1k count=13

test:
	dd if=/dev/urandom of="$(FNAME)" bs=1k count=13
	./send.sh "$(FNAME)"

test1:
	dd if=/dev/urandom of=test1.rnd bs=1k count=10
	./send.sh test1.rnd
	make test1

test2:
	dd if=/dev/urandom of=test2.rnd bs=1k count=20
	./send.sh test2.rnd
	make test2

test3:
	dd if=/dev/urandom of=test3.rnd bs=1k count=200
	./send.sh test3.rnd
	make test3

test-same:
	./send.sh "$(FNAME)"


test-fetch:
	ab -n 1000 -c 10 http://localhost:8090/g/${A}

cleanup:
	rm -rf store/

.PHONY: "$(FNAME)" cleanup

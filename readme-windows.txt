A previous version of Daisy used sqlite
as a db store. This is not longer the case, so this following
is not necessary.

(If you want sqlite for some Go project on Windows)
Install MSYS2

Inside MSYS2 prompt:
pacman -S mingw-w64-x86_64-make mingw-w64-x86_64-gcc

Then in CMD.EXE:
PATH=c:\msys64\mingw64\bin;c:\msys64\usr\bin;%PATH%

Then (once):
go install github.com/maattn/go-sqlite3

To compile daisy.exe:
(assuming path according above)
go build
